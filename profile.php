<!DOCTYPE html>
<?php 
$activePage = "Profil";
include_once "includes/header.php"; 
?>
<html lang="en">
<body>
<?php
if(!isset($_SESSION['user'])){
    header("Location: login.php");
}?>
<div class="container">
        <div class="card">
            <div class="card-header text-sm-center">
                <h3>Twoje dane:</h3>
            </div>
            <div class="card-body">
                Login: <?= $_SESSION['user']->username;?><br>
                Imię: <?= $_SESSION['user']->name;?><br>
                Nazwisko: <?= $_SESSION['user']->surname;?><br>
                Email: <?= $_SESSION['user']->email;?><br><br>
                <h6><a href="changepass.php">Zmień hasło.</a></h6>
            </div>
        </div>
    </div>
</body>
</html>