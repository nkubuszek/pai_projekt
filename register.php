<!DOCTYPE html>
<html lang="en">

<?php 
$activePage = "Rejestracja";
include_once "includes/header.php";
if(isset($_POST['submit'])){
    $user->register($_POST);
}

if(isset($_SESSION['user'])){
    header("Location: index.php");
}
?>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header text-sm-center">
                <h3>Zarejestruj się:</h3>
            </div>
            <div class="card-body">
                <?php
                    if(isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $_SESSION['error'];
                        echo '</div>';
                    }
                ?>
                <form method="post">
                    <input type="text" class="form-control" id="nazwa" name="nazwa" placeholder="Nazwa użytkownika" value="<?php echo @$_POST['nazwa'];?>" required autofocus>
                    <input type="password" class="form-control" id="haslo" name="haslo" placeholder="Hasło"  required>
                    <input type="password" class="form-control" id="haslo2" name="haslo2" placeholder="Powtórz hasło" required>
                    <input type="text" class="form-control" id="imie" name="imie" placeholder="Imię" value="<?php echo @$_POST['imie'];?>" required>
                    <input type="text" class="form-control" id="nazwisko" name="nazwisko" placeholder="Nazwisko" value="<?php echo @$_POST['nazwisko'];?>" required>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo @$_POST['email'];?>" required>
                    <input class="btn btn-primary" type="submit" value="Zarejestruj" name="submit">
                </form>    
            <h6>Masz już konto? <a href="login.php">Zaloguj się.</a></h6>
            </div>
        </div>
    </div>
        
</body>
</html>