<!DOCTYPE html>
<html lang="en">

<?php 
$activePage = "Logowanie";
include_once "includes/header.php"; 

if(isset($_POST['submit'])){
    $user->login($_POST);
}

?>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header text-sm-center">
                <h3>Zaloguj się:</h3>
            </div>
            <div class="card-body">
                <?php
                    if(isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $_SESSION['error'];
                        echo '</div>';
                    }
                ?>
                <form method="post">
                    <input type="text" class="form-control" id="nazwa" name="nazwa" placeholder="Nazwa użytkownika" required autofocus>
                    <input type="password" class="form-control" id="haslo" name="haslo" placeholder="Hasło" required>
                    <input class="btn btn-primary" type="submit" value="Zaloguj" name="submit">
                </form>       
            <h6>Nie masz konta? <a href="register.php">Zarejestruj się.</a></h6>
        </div>
    </div>
</div>
        
</body>
</html>