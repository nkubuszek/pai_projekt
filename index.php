<!DOCTYPE html>
<?php 
$activePage = "Strona główna";
include_once "includes/header.php"; 
?>
<html lang="en">
<body>
    <?php
    if(isset($_SESSION['success'])){
        echo '<div class="alert alert-success" role="alert">';
        echo $_SESSION['success'];
        echo '</div>';
        $_SESSION['success'] = NULL;
    }
    ?>
    <div class="container">
        <h2 class"home">Najnowsze posty</h2>
        <?php
        if(!isset($_SESSION['user'])){
            header("Location: login.php");
        }
        include_once "posts.php";
        ?>
    </div>
</body>
</html>