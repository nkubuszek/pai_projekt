<!DOCTYPE html>
<html lang="en">

<?php 
$activePage = "Zmiana hasła";
include_once "includes/header.php"; 

if(isset($_POST['submit'])){
    $user->changePassword($_POST['haslo'], $_POST['nowehaslo'], $_POST['nowehaslo2']);
}

?>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header text-sm-center">
                <h3>Podaj stare i nowe hasło:</h3>
            </div>
            <div class="card-body">
                <?php
                    if(isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $_SESSION['error'];
                        echo '</div>';
                    }
                ?>
                <form method="post">
                    <input type="password" class="form-control" id="haslo" name="haslo" placeholder="Obecne hasło" required autofocus>
                    <input type="password" class="form-control" id="nowehaslo" name="nowehaslo" placeholder="Nowe hasło" required>
                    <input type="password" class="form-control" id="nowehaslo2" name="nowehaslo2" placeholder="Powtórz nowe hasło" required>
                    <input class="btn btn-primary" type="submit" value="Zatwierdź" name="submit">
                </form>
        </div>
    </div>
</div>
        
</body>
</html>