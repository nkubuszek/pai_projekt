<!DOCTYPE html>
<html lang="en">

<?php 
$activePage = "Nowy post";
include_once "classes/Post.php";
include_once "includes/header.php";
$post = new Post();
if(isset($_POST['submit'])){
    $post->addPost($_POST);
}

if(!isset($_SESSION['user'])){
    header("Location: login.php");
}
?>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header text-sm-center">
                <h3>Dodaj nowy post:</h3>
            </div>
            <div class="card-body">
                <?php
                    if(isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $_SESSION['error'];
                        echo '</div>';
                    }
                ?>
                <form method="post">
                    <input type="text" class="form-control" id="tytul" name="tytul" placeholder="Tytuł posta" value="<?php echo @$_POST['tytul'];?>" required autofocus>
                    <textarea name="opis" class="form-control" placeholder="Krótki opis (max. 255 znaków)" required></textarea>
                    <textarea name="tresc" class="form-control" rows='7' placeholder="Treść posta" required></textarea>
                    
                    <input class="btn btn-primary" type="submit" value="Dodaj" name="submit">
                </form> 
            </div>
        </div>
    </div>
        
</body>
</html>