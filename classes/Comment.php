<?php

class Comment extends mysqli{

    function __construct(){
        Parent::__construct('localhost', 'root', '', 'pai');

        if($this->connect_error){
            $_SESSION['error'] = "Błąd połączenia z bazą danych: ".$this->connect_error;
            return;
        }
        $_SESSION['error'] = NULL;
    }

    public function addComment($comment, $postId){
        $id = json_decode($_SESSION['user']->id, true);
        $query = "INSERT INTO `comments` (`post_id`, `user_id`, `content`) VALUES ('$postId', '$id', '$comment')";
        $result = $this->query($query);

        if($result){
            $_SESSION['success'] = "Komentarz dodany pomyślnie.";
        }
        else{
            $_SESSION['error'] = "Nie udało się dodać komentarza.";
        }
    }

    public function getComments($postId){
        $query = "SELECT posts.id, users.username, comments.content, comments.add_date FROM comments 
                    INNER JOIN users ON users.id=comments.user_id 
                    INNER JOIN posts ON posts.id=comments.post_id ORDER BY comments.add_date DESC";
        $result = $this->query($query);
        return $result->fetch_all();
    }
}