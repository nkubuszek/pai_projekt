<?php

class User extends mysqli{

    function __construct(){
        Parent::__construct('localhost', 'root', '', 'pai');

        if($this->connect_error){
            $_SESSION['error'] = "Błąd połączenia z bazą danych: ".$this->connect_error;
            return;
        }
        $_SESSION['error'] = NULL;
    }

    public function register($data){
        $query = "SELECT * FROM users WHERE email='$data[email]' or username='$data[nazwa]'";
        $result = $this->query($query);
        $haslo = password_hash($data['haslo'], PASSWORD_DEFAULT);

        if ($result->num_rows > 0){
            $_SESSION['error'] = "Email lub nazwa użytkownika już jest w użyciu.";
            return;
        }
        elseif($data['haslo'] != $data['haslo2']){
            $_SESSION['error'] = "Podane hasła się nie zgadzają.";
            return;
        }
        else{
            $query = "INSERT INTO users(username, password, name, surname, email) VALUES ('$data[nazwa]','$haslo','$data[imie]','$data[nazwisko]','$data[email]')";
            $result = $this->query($query);
            if($result){
                $_SESSION['success'] = "Rejestracja zakończona powodzeniem!";
                $user = $this->getUserData($data['email']);
                $_SESSION['user'] = $user;
            }
            else{
                $_SESSION['error'] = "Rejestracja się nie powiodła.";
                return;
            }
        }
    }

    public function getUserData($email){
        $query = "SELECT * FROM users WHERE email='$email'";
        $result = $this->query($query);
        return $result->fetch_object();
    }

    public function login($data){
        $query = "SELECT * FROM users WHERE username='$data[nazwa]'";
        $results = $this->query($query);

        if($results->num_rows > 0){
            $user = $results->fetch_object();
            if(password_verify($data['haslo'], $user->password)){
                $_SESSION['user'] = $user;
                header("Location: index.php");
            }
            else{
                $_SESSION['error'] = "Podane hasło jest błędne.";
            }
        }
        else{
            $_SESSION['error'] = "Nie ma takiego użytkownika.";
        }
    }

    public function changePassword($oldpass, $newpass, $newpass2){
        $id = json_decode($_SESSION['user']->id, true);

            if(password_verify($oldpass, $_SESSION['user']->password)){
                if($newpass2 != $newpass){
                    $_SESSION['error'] = "Podane hasła się nie zgadzają.";
                    return;
                }
                $newpass = password_hash($newpass, PASSWORD_DEFAULT);
                $query = "UPDATE `users` SET password = '$newpass' WHERE `users`.`id` = '$id'";
                $result = $this->query($query);
                if($result){
                    $_SESSION['success'] = "Hasło zostało zmienione.";
                    header("Location: index.php");
                }
            }
        }
    
}