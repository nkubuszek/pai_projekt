<?php

class Post extends mysqli{

    function __construct(){
        Parent::__construct('localhost', 'root', '', 'pai');

        if($this->connect_error){
            $_SESSION['error'] = "Błąd połączenia z bazą danych: ".$this->connect_error;
            return;
        }
        $_SESSION['error'] = NULL;
    }

    public function addPost($data){
        $id = json_decode($_SESSION['user']->id, true);
        $query = "INSERT INTO `posts` (`user_id`, `title`, `description`, `content`) VALUES ('$id', '$data[tytul]', '$data[opis]', '$data[tresc]')";
        $result = $this->query($query);

        if($result){
            $_SESSION['success'] = "Post dodany pomyślnie.";
            header("Location: index.php");
        }
        else{
            $_SESSION['error'] = "Nie udało się dodać posta.";
        }
    }

    public function getPosts(){
        $query = "SELECT posts.id, title, description, users.username FROM posts INNER JOIN users ON users.id=posts.user_id ORDER BY add_date DESC";
        $result = $this->query($query);
        return $result->fetch_all();
    }

    public function getPost($id){
        $query = "SELECT posts.id, title, description, content, users.username FROM posts INNER JOIN users ON users.id=posts.user_id WHERE posts.id = '$id'";
        $result = $this->query($query);
        return $result->fetch_assoc();
    }
}
?>