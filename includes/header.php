<?php
session_start();
include_once "classes/User.php";
$user = new User();
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?php echo $activePage;?></title>

    <div class="topnav" id="myTopnav">
        <a href="index.php" <?php if ($activePage == "Strona główna") {echo "class=\"active\"";}?> >Strona główna</a>
        <?php if(!isset($_SESSION['user'])){?>
        <a href="login.php" <?php if ($activePage == "Logowanie") {echo "class=\"active\"";}?>>Logowanie</a>
        <a href="register.php" <?php if ($activePage == "Rejestracja") {echo "class=\"active\"";}?>>Rejestracja</a>
    
        <?php }else{?>
        <a href="newpost.php" <?php if ($activePage == "Nowy post") {echo "class=\"active\"";}?>>Dodaj</a>
        <a href="logout.php" <?php if ($activePage == "Wyloguj") {echo "class=\"active\"";}?>>Wyloguj</a>
        <a href="profile.php" <?php if ($activePage == "Profil") {echo "class=\"active\"";}?>>Profil</a>
        <?php }?>
    </div>
</head>
