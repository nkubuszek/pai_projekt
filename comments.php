<?php 
$comments = $comment->getComments($currentPost['id']);?>
<div class="commentBox">
    <ul class="commentList">
        <?php
        foreach ($comments as $c){?>
        <li>
            <div class="commentText">
                <?= $c[2];?>
            </div>
            <div class="commentAuthor">
                Autor: <?= $c[1];?>, <?= $c[3];?>
            </div>
        </li>
        <?php }?>
    </ul>
</div>