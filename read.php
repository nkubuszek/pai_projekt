<!DOCTYPE html>
<?php 
include_once "classes/Post.php";
include_once "classes/Comment.php";
$post = new Post();
$comment = new Comment();

if(isset($_GET['post'])){
    $currentPost = $post->getPost($_GET['post']);
}
else{
    header("Location: index.php");
}

$activePage = "Strona główna";
include_once "includes/header.php";

if(isset($_POST['submit'])){
    $comment->addComment($_POST['komentarz'], $currentPost['id']);
}
?>
<html lang="en">
<body>
<div class="container">
    <div class="jumbotron jumbotron-fluid">
        
        <h1 class="display-4"><?= $currentPost['title'];?></h1>
        <h6><?= "dodany przez ".$currentPost['username'];?><h6>
        <p class="lead"><?= $currentPost['content'];?></p>
    </div>
    <div class="comments">
        <div class="card">
            <div class="card-header text-sm-left">
                <h4>Skomentuj:</h4>
            </div>
            <div class="card-body">
                <?php
                    if(isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $_SESSION['error'];
                        echo '</div>';
                    }
                    if(isset($_SESSION['success'])){
                        echo '<div class="alert alert-success" role="alert">';
                        echo $_SESSION['success'];
                        echo '</div>';
                        $_SESSION['success'] = NULL;
                    }
                ?>
                <form method="post">
                    <textarea name="komentarz" class="form-control" rows='3' placeholder="Twój komentarz" required></textarea>
                    
                    <input class="btn btn-primary" type="submit" value="Dodaj" name="submit">
                </form> 
            </div>
        </div>

    <?php include_once "comments.php";?>

</div>


    
</body>
</html>