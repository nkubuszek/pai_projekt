<?php
include_once "classes/Post.php";
$post = new Post();
$posts = $post->getPosts();

foreach ($posts as $p){?>
<div class="card">
    <h5 class="card-header"><?= $p[1];?></h5>
    <div class="card-body">
        <h5 class="card-title"><?= $p[2];?></h5>
        <p class="card-text">Dodany przez: <?= $p[3];?></p>
        <a href="read.php?post=<?= $p[0];?>" class="btn btn-primary">Czytaj</a>
    </div>
</div>
<?php }?>